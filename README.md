Deploy To Heroku
================
* Username: uscomics@protonmail.com

To deploy
---------
* heroku login
* git push heroku master
  
BitBucket
=========
* Username: magicjtv@gmail.com
  
* WASM Resources
--------------
* [Rust 🦀 and WebAssembly 🕸 (tutorial)](https://rustwasm.github.io/docs/book/game-of-life/introduction.html)
* [Up and Running with React + Rust + Wasm (blogpost)](https://prestonrichey.com/blog/react-rust-wasm/)
* [hero_core-wasm](https://www.npmjs.com/settings/magicjava/packages)

React Resources
---------------
* [Using WebAssembly with React](https://www.telerik.com/blogs/using-webassembly-with-react)
* [react-awesome-router](https://github.com/hryuk/react-awesome-router)
* [React Draggable](http://mzabriskie.github.io/react-draggable/example/)
* [react-query](https://www.npmjs.com/package/react-query)
* [Pullstate](https://lostpebble.github.io/pullstate/)
* [Deploy Your React App To Heroku](https://dev.to/smithmanny/deploy-your-react-app-to-heroku-2b6f)

WebPack Resources
-----------------
* [Inserting variables into HTML and JavaScript with Webpack](https://medium.com/dailyjs/inserting-variables-into-html-and-javascript-with-webpack-80f33625edc6)

Tailwind Resources
------------------
* [Setting up Tailwind and PostCSS](https://tailwindcss.com/course/setting-up-tailwind-and-postcss)
* [Using Tailwind CSS with Create React App](https://daveceddia.com/tailwind-create-react-app/)
  