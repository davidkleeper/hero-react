import React, { useState } from "react";
import BasicInfo from "../components/BasicInfo";
import Build from "../components/Build";
import {fetchDataFromServer} from '../fetch/fetch'
import PowerGrid from "../components/PowerGrid";
import PowerPicker from "../components/PowerPicker";
import PowerSetPicker from "../components/PowerSetPicker";
import { PowerSetStore } from '../store/PowerSetStore';
import TabBar from "../components/TabBar";
import { ToonStore } from '../store/ToonStore';
import StatusBar from '../components/StatusBar'
import { useLocation } from 'react-awesome-router';
import './toon.scss';
  
function Toon(props){
    // WASM support
    let {context, params, setContext, setLocation} = useLocation();
    let wasm = context.wasm
    if (!wasm){
        setLocation('/Splash'); 
        window.location.reload();
        return;
    }

    // State
    const [powerSetPickerVisible, setPowerSetPickerVisible] = useState(true)
    const [powerPickerVisible, setPowerPickerVisible] = useState(false)
    const [selectedPrimaryPowerSet, setSelectedPrimaryPowerSet] = useState("")
    const [selectedSecondaryPowerSet, setSelectedSecondaryPowerSet] = useState("")
    const [toon, setToon] = useState(null)

    // Toon store
    const updateToon = (toonJSON) => {
        let toonObj = (Object.prototype.toString.call(toonJSON) === "[object String]")? JSON.parse(toonJSON) : toonJSON
        ToonStore.update(s => { s.toon = toonObj; })
        setToon(toonObj)
    }

    // Appearance
    let hidePickers = () => {
        hidePowerSetPicker()
    }
    let showPowerSetPicker = () => {
        if (!powerSetPickerVisible) 
            setPowerSetPickerVisible(true)
    }
    let hidePowerSetPicker = () => {
        if (powerSetPickerVisible) 
            setPowerSetPickerVisible(false)
    }
    const getPowerSetPickerClass = () => {
        if (powerSetPickerVisible) return ''
        else return 'Hidden'
    }
    const getPowerPickerClass = () => {
        if (powerPickerVisible) return ''
        else return 'Hidden'
    }

    // Click events
    const onPowerSetClicked = (primaryId, primaryIndex, secondaryId, secondaryIndex) => {
        setSelectedPrimaryPowerSet(primaryId)
        setSelectedSecondaryPowerSet(secondaryId)
        showPowerSetPicker()
    }

    // Key events
    // Closes the pickers when ESC is pressed
    const onKeyDown = (event) => {
        console.log('onKeyDown')
        let evt = event || window.event;
        if (evt.keyCode === 27) {
            console.log('ESC')
            hidePickers()
        }
    }

   // Functions for PowerSetPicker
   const powerSetPickerCallback = () => {
        setPowerSetPickerVisible(false)
    }
  
    // Load Toon
    if (!toon || !toon.archetype_id || 0 === toon.archetype_id.length){
        let toonJSON = wasm.core_command_web("toon_new!@#" + params.archetype)
        updateToon(toonJSON)
    } 

    return (
        <div 
            className={"Toon"}
            onKeyDown={ onKeyDown }
            tabIndex="0"
        >
            <div className={getPowerSetPickerClass()}>
                <PowerSetPicker powerSetPickerCallback={powerSetPickerCallback}/>
            </div>
            <div className={getPowerPickerClass()}>
                <PowerPicker 
                    powerSetPickerCallback={powerSetPickerCallback}
                    primary_set_id={selectedPrimaryPowerSet}
                    secondary_set_id={selectedSecondaryPowerSet}
                />
            </div>
            <div className="ToonGeneral">
                <BasicInfo onPowerSetClicked={onPowerSetClicked}/>
                <Build toon={toon}/>
            </div>
            <TabBar/>
            <PowerGrid/>
            <StatusBar/>
        </div>
    );
}

export default Toon;