
import React from 'react';
import initErrorMessageDisplay from '../components/ErrorMessageDisplay'
import initInfoMessageDisplay from '../components/InfoMessageDisplay'
import { pingServers } from '../fetch/fetch'
import { useLocation } from 'react-awesome-router';
import './splash.scss';

let initialized = false

function Splash(props){
  if (!initialized) {
    initialized = true
    initErrorMessageDisplay()
    initInfoMessageDisplay()
    pingServers()
    const {setLocation, setContext} = useLocation()
    setTimeout(()=>{ 
      setLocation('/Home')
    }, 3000);
    loadWasm(setContext)
  }

  return (
    <div className="splash-page">
     <div className="splash-title">HERO/Web</div>
    </div>
  );
}

let loadWasm = async (setContext) => {
  try {
      console.log("Loading WASM");
      const wasm = await import('hero_core-wasm');
      setContext({wasm});
  } catch(err) {
      console.error(`Unexpected error in loadWasm. [Message: ${err.message}]`);
  }
};

export default Splash;
