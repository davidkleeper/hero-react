import { Store } from "pullstate";

export const PowerSetStore = new Store({
    primaryPowerSets: [],
    secondaryPowerSets: [],
    inherentPowerSets: [],
    poolPowerSets: [],
    epicPowerSets: [],
    pickerVisible: false,
    selectedPrimary: '',
    selectedPSecondary: '',
    powerSetInfo: {}
});