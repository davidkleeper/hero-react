import { Store } from "pullstate";

export const InfoMessageStore = new Store({
  infoMessage: ''
});