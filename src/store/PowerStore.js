import { Store } from "pullstate";

export const PowerStore = new Store({
    powerSets: [],
    powers: [],
});