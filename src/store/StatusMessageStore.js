import { Store } from "pullstate";

export const StatusMessageStore = new Store({
  statusMessage: ''
});