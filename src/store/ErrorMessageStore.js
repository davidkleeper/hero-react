import { Store } from "pullstate";

export const ErrorMessageStore = new Store({
  errorMessage: ''
});