import { ErrorMessageStore } from '../store/ErrorMessageStore';
import { StatusMessageStore } from '../store/StatusMessageStore';

let SERVERS = ['https://gentle-dusk-67062.herokuapp.com/', 'https://gentle-dusk-67062.herokuapp.com/']
// let SERVERS = ['http://localhost:8080/', 'http://localhost:8080/']
let fetchDataFromServer = async (urlFragment) => {
    let params = {
        method: 'GET',
        mode: 'cors',
        cache: 'no-cache',
        headers: { 'Accept': 'application/json' }
    };
    return serverCommunication(urlFragment, params)
}

let postDataToServerWithJSONResponse = async (urlFragment, data) => {
    let params = {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
        body: data
    };
    return serverCommunication(urlFragment, params)
}

let serverCommunication = async (urlFragment, params) => {
    let url = SERVERS[0] + urlFragment
    let response = await fetch(url, params);
    let json = await response.json()
    return json
    /*
      try {
        let url = SERVERS[0] + urlFragment
        StatusMessageStore.update(s => { s.statusMessage = 'Fetching ' + url; })
        let response = (params)? await fetch(url, params) : await fetch(url)
        if (response.ok) {
            let json = await response.json()
            StatusMessageStore.update(s => { s.statusMessage = 'Fetch of ' + url + ' successful'; })
            return json
        } else {
            toggleServers()
            let url = SERVERS[0] + urlFragment
            StatusMessageStore.update(s => { s.statusMessage = 'Fetch failed. Trying ' + url; })
            let response = (params)? await fetch(url, params) : await fetch(url)
            if (response.ok) {
                let json = await response.json()
                StatusMessageStore.update(s => { s.statusMessage = 'Fetch of ' + url + ' successful'; })
                return json
            } else {
                ErrorMessageStore.update(s => { s.errorMessage = 'Could not fetch ' + urlFragment})
                return null
            }
        }
    } catch (e) {
        console.error('Failed to fetch ' + urlFragment + '. Error is ' + e)
    }
    */
}

let pingServer = async (baseURL) => {
    let response = await fetch(baseURL + 'ping/server')
    if (response.ok) return true
    return false
}
let pingServers = async () => {
    StatusMessageStore.update(s => { s.statusMessage = 'Pinging API servers.'; })
    console.log('Ping: ' + await pingServer(SERVERS[0]))
    console.log('Ping: ' + await pingServer(SERVERS[1]))
}

let toggleServers = () => {
    let s = SERVERS[0]
    SERVERS[0] = SERVERS[1]
    SERVERS[1] = s
}
export { fetchDataFromServer, pingServers, postDataToServerWithJSONResponse }