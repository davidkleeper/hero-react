import React, { useRef, useState, useEffect } from 'react'
import {fetchDataFromServer} from '../fetch/fetch'
import { PowerSetStore } from '../store/PowerSetStore';
import { SVGInfoButtonLight, SVGStandardCircleButtonStyle } from './SVG/SVGCircleButton'
import TextPicker from "./TextPicker";
import { ToonStore } from '../store/ToonStore';
import { useLocation } from 'react-awesome-router';
import './PowerSetPicker.scss';

// State
//    Primary power sets
//    Secondary power sets
//    Selected primary id and store index
//    Selected secondary id and store index
//
// Store
//    Toon Store: When Select clicked, Toon store updated with selected primary and secondary. 
//    PowerSetStore: When Select or Cancel clicked, visible state set to false and Power Set store updated 
//      with pickerVisible set to false. 
//
// Use Effect
//    Load primary and secondary power sets. Store them in Power Set Store.
//    Drag support
//
// Props
//    powerSetPickerCallback - Called after the power sets are selected. Used to close the picker.
const PowerSetPicker = (props) => {
  // WASM support
  let {context} = useLocation();
  let wasm = context.wasm

  // Drag support
  const [drag_support_pressed, set_drag_support_pressed] = useState(false)
  const [drag_support_position, set_drag_support_position] = useState({x: 0, y: 0})
  const onMouseMove = (event) => {
    if (drag_support_pressed) {
      set_drag_support_position({
        x: drag_support_position.x + event.movementX,
        y: drag_support_position.y + event.movementY
      })
    }
  }

  // State
  const [primaryPowerSets, setPrimaryPowerSets] = useState(PowerSetStore.primaryPowerSets)
  const [secondaryPowerSets, setSecondaryPowerSets] = useState(PowerSetStore.secondaryPowerSets)
  const [selectedPrimaryPowerSetId, setSelectedPrimaryPowerSetId] = useState('')
  const [selectedPrimaryPowerSetIndex, setSelectedPrimaryPowerSetIndex] = useState(-1)
  const [selectedSecondaryPowerSetId, setSelectedSecondaryPowerSetId] = useState('')
  const [selectedSecondaryPowerSetIndex, setSelectedSecondaryPowerSetIndex] = useState(-1)
  const ref = useRef()

  // Toon store
  const toon = ToonStore.useState(s => s.toon);
  const updateToon = (toonJSON) => {
    let toonObj = (Object.prototype.toString.call(toonJSON) === "[object String]")? JSON.parse(toonJSON) : toonJSON
    ToonStore.update(s => { s.toon = toonObj; })
  }

  // Power Set Store
  PowerSetStore.subscribe( 
    s => s.primaryPowerSets, 
    newPrimaryPowerSets => {
      setPrimaryPowerSets(newPrimaryPowerSets)
    }
  )
  PowerSetStore.subscribe( 
    s => s.secondaryPowerSets, 
    newSecondaryPowerSets => {
      setSecondaryPowerSets(newSecondaryPowerSets)
    }
  )

  // Use Effect
  // Power Set Store: Fetch primary and secondary power sets.
  // Drag Support: Monitor changes to position state and update DOM.
  useEffect(() => {
    fetchPrimaryPowerSets(toon.archetype_id)
    fetchSecondaryPowerSets(toon.archetype_id)
    if (ref.current) {
      ref.current.style.transform = `translate(${drag_support_position.x}px, ${drag_support_position.y}px)`
    }
  }, [drag_support_position])

  // Appearance
  let getSelectButtonClass = () => {
    if (-1 === selectedPrimaryPowerSetIndex || -1 === selectedSecondaryPowerSetIndex)
      return 'PowerSetPickerButtonDisabled'
    else
      return 'PowerSetPickerButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... '
  }

  // Click events
  let onPrimaryClicked = (id, index) => {
    setSelectedPrimaryPowerSetId(id)
    setSelectedPrimaryPowerSetIndex(index)
  }
  let onSecondaryClicked = (id, index) => {
    setSelectedSecondaryPowerSetId(id)
    setSelectedSecondaryPowerSetIndex(index)
  }
  let onCancelClicked = (id, index) => {
    PowerSetStore.update(s => { s.pickerVisible = false })
    props.powerSetPickerCallback()
  }
  let onSelectClicked = () => {
    if (-1 === selectedPrimaryPowerSetIndex || -1 === selectedSecondaryPowerSetIndex)
      return

    let dataArray = [];
    dataArray.push(JSON.stringify(toon))
    dataArray.push(selectedPrimaryPowerSetId)
    let updatedToon = wasm.core_command_web("toon_set_primary!@#" + JSON.stringify(dataArray))
    dataArray[0] = updatedToon
    dataArray[1] = selectedSecondaryPowerSetId
    updatedToon = wasm.core_command_web("toon_set_secondary!@#" + JSON.stringify(dataArray))
    updateToon(updatedToon)
    props.powerSetPickerCallback()
  }
  let onInfoClicked = (id, index) => {
    alert('Info')
  }

  // Functions for TextPicker
  let getArchetypeId = (data, index) => {
    return data[index].id
  }
  let getArchetypeDisplayName = (data, index) => {
    return data[index].display_name
  }

  return (
    <div
        className="PowerSetPicker"
        ref={ ref }
        onMouseMove={ onMouseMove }
        onMouseDown={ () => set_drag_support_pressed(true) }
        onMouseUp={ () => set_drag_support_pressed(false) }
       >
        <div className="PowerSetPickerTitlebar">
            <div className="PowerSetPickerTitle">Select Power Sets</div>
            <div className={SVGStandardCircleButtonStyle()} onClick={onInfoClicked}><SVGInfoButtonLight/></div> 
        </div>
        <div className="PowerSetPickerBody">
            <TextPicker 
              data={primaryPowerSets} 
              getTextFunc={getArchetypeDisplayName} 
              getIdFunc={getArchetypeId} 
              itemClickCallback={onPrimaryClicked}
              title="Primary"
            />
            <TextPicker 
              data={secondaryPowerSets} 
              getTextFunc={getArchetypeDisplayName} 
              getIdFunc={getArchetypeId} 
              itemClickCallback={onSecondaryClicked}
              title="Secondary"
            />
        </div>
        <div className="PowerSetPickerButtons">
          <div className="PowerSetPickerButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... " onClick={onCancelClicked}>Cancel</div>
          <div className={getSelectButtonClass()} onClick={onSelectClicked}>Select</div>
        </div>
    </div>
  )
}

let fetchPrimaryPowerSets = async (archetype_id) => {
  let data = await fetchDataFromServer('primary_power_sets_for_archetype/' + archetype_id)
  PowerSetStore.update(s => { s.primaryPowerSets = data })
}

let fetchSecondaryPowerSets = async (archetype_id) => {
  let data = await fetchDataFromServer('secondary_power_sets_for_archetype/' + archetype_id)
  PowerSetStore.update(s => { s.secondaryPowerSets = data })
}

export default PowerSetPicker