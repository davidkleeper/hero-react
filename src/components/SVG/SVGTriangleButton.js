import * as React from "react";

function SVGTriangleButton(props) {
  const getWidth = (props) => { return (props.width)? props.width : 100 }
  const getHeight = (props) => { return (props.height)? props.height : 100 }
  const getCX = (props) => { return (props.cx)? props.cx : 50 }
  const getCY = (props) => { return (props.cy)? props.cy : 50 }
  const getR = (props) => { return (props.r)? props.r : 40 }
  const getCircleFill = (props) => { return (props.circleFill)? props.circleFill : "white" }
  const getCircleStroke = (props) => { return (props.circleStroke)? props.circleStroke : "black" }
  const getCircleStrokeWidth = (props) => { return (props.circleStrokeWidth)? props.circleStrokeWidth : "3" }
  const getCircleStrokeDashArray = (props) => { return (props.circleStrokeDashArray)? props.circleStrokeDashArray : 0 }
  const getTriangleX = (props) => { return (props.textX)? props.textX : "50" }
  const getTriangleY = (props) => { return (props.textY)? props.textY : "50" }
  const getTriangleFill = (props) => { return (props.triangleFill)? props.triangleFill : "black" }
  const getTriangleStroke = (props) => { return (props.triangleStroke)? props.triangleStroke : "black" }
  const getTriangleStrokeWidth = (props) => { return (props.triangleStrokeWidth)? props.triangleStrokeWidth : 1 }
  const getTrianglePoints = (props) => { return (props.trianglePoints)? props.trianglePoints : "50,15 25,75 75,75" }
  return (
      <svg height={getHeight(props)} width={getWidth(props)}>
          <circle 
              cx={getCX(props)} 
              cy={getCY(props)}
              r={getR(props)}
              stroke={getCircleStroke(props)}
              strokeWidth={getCircleStrokeWidth(props)}
              fill={getCircleFill(props)}
              strokeDasharray={getCircleStrokeDashArray(props)}
          />
          <polygon 
            points={getTrianglePoints(props)}
            x={getTriangleX(props)}
            y={getTriangleY(props)}
            fill={getTriangleFill(props)}
            stroke={getTriangleStroke(props)}
            strokeWidth={getTriangleStrokeWidth(props)}
          />
      </svg>
   )
}

const dark = "#555555"
const light = "#FFFFFF"
const disabled = "#999999"

function StandardSVGTriangleButton(triangleStroke, triangleFill, circleStroke, circleFill) {
  return (
      <SVGTriangleButton
          width={25}
          height={25}
          cx={12}
          cy={12}
          r={10}
          circleStroke={circleStroke}
          circleStrokeWidth={1}
          circleFill={circleFill}
          triangleStrokeWidth={1}
          triangleStroke={triangleStroke}
          triangleFill={triangleFill}
          trianglePoints="7,17 12,6 18,17"
      />
  )
}

function FlippedSVGTriangleButton(triangleStroke, triangleFill, circleStroke, circleFill) {
  return (
      <SVGTriangleButton
          width={25}
          height={25}
          cx={12}
          cy={12}
          r={10}
          circleStroke={circleStroke}
          circleStrokeWidth={1}
          circleFill={circleFill}
          triangleStrokeWidth={1}
          triangleStroke={triangleStroke}
          triangleFill={triangleFill}
          trianglePoints="7,7 12,18 18,7"
      />
  )
}

function SVGStandardTriangleButtonStyle() {
  return "SmallButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... "
}

function SVGTriangleButtonLight() {
  return StandardSVGTriangleButton(dark, dark, dark, light)
}

function SVGTriangleButtonLightDisabled() {
  return StandardSVGTriangleButton(disabled, disabled, disabled, light)
}

function SVGTriangleButtonDark() {
  return StandardSVGTriangleButton(light, light, dark, dark)
}

function SVGTriangleButtonDarkDisabled() {
  return StandardSVGTriangleButton(light, light, disabled, disabled)
}

function SVGFlippedTriangleButtonLight() {
  return FlippedSVGTriangleButton(dark, dark, dark, light)
}

function SVGFlippedTriangleButtonLightDisabled() {
  return FlippedSVGTriangleButton(disabled, disabled, disabled, light)
}

function SVGFlippedTriangleButtonDark() {
  return FlippedSVGTriangleButton(light, light, dark, dark, dark)
}

function SVGFlippedTriangleButtonDarkDisabled() {
  return FlippedSVGTriangleButton(light, light, disabled, disabled)
}

export default SVGTriangleButton;
export { StandardSVGTriangleButton, SVGStandardTriangleButtonStyle };
export { SVGTriangleButtonLight, SVGTriangleButtonLightDisabled, SVGTriangleButtonDark, SVGTriangleButtonDarkDisabled };
export { SVGFlippedTriangleButtonLight, SVGFlippedTriangleButtonLightDisabled, SVGFlippedTriangleButtonDark, SVGFlippedTriangleButtonDarkDisabled };
