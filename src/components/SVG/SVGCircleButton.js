import * as React from "react";

function SVGCircleButton(props) {
    const getWidth = (props) => { return (props.width)? props.width : 100 }
    const getHeight = (props) => { return (props.height)? props.height : 100 }
    const getCX = (props) => { return (props.cx)? props.cx : 50 }
    const getCY = (props) => { return (props.cy)? props.cy : 50 }
    const getR = (props) => { return (props.r)? props.r : 40 }
    const getCircleFill = (props) => { return (props.circleFill)? props.circleFill : "white" }
    const getCircleStroke = (props) => { return (props.circleStroke)? props.circleStroke : "black" }
    const getCircleStrokeWidth = (props) => { return (props.circleStrokeWidth)? props.circleStrokeWidth : "3" }
    const getCircleStrokeDashArray = (props) => { return (props.circleStrokeDashArray)? props.circleStrokeDashArray : 0 }
    const getText = (props) => { return (props.text)? props.text : "" }
    const getTextX = (props) => { return (props.textX)? props.textX : "50" }
    const getTextY = (props) => { return (props.textY)? props.textY : "50" }
    const getTextFill = (props) => { return (props.textFill)? props.textFill : "black" }
    const getTextFontFamily = (props) => { return (props.textFontFamily)? props.textFontFamily : "sans-serif" }
    const getTextFontSize = (props) => { return (props.textFontSize)? props.textFontSize : "24pt" }
    const getTextFontWeight = (props) => { return (props.textFontWeight)? props.textFontWeight : "normal" }
    const getTextStrokeWidth = (props) => { return (props.textStrokeWidth)? props.textStrokeWidth : 1 }
    return (
        <svg height={getHeight(props)} width={getWidth(props)}>
            <circle 
                cx={getCX(props)} 
                cy={getCY(props)}
                r={getR(props)}
                stroke={getCircleStroke(props)}
                strokeWidth={getCircleStrokeWidth(props)}
                fill={getCircleFill(props)}
                strokeDasharray={getCircleStrokeDashArray(props)}
            />
            <text 
                dominantBaseline="middle" 
                textAnchor="middle"
                x={getTextX(props)}
                y={getTextY(props)}
                fill={getTextFill(props)}
                fontFamily={getTextFontFamily(props)}
                fontSize={getTextFontSize(props)}
                fontWeight={getTextFontWeight(props)}
                strokeWidth={getTextStrokeWidth(props)}
            >
                {getText(props)}
            </text>
        </svg>
     )
}

const dark = "#555555"
const light = "#FFFFFF"
const disabled = "#999999"

function StandardSVGCircleButton(text, textFill, circleStroke, circleFill) {
    return (
        <SVGCircleButton
            width={25}
            height={25}
            cx={12}
            cy={12}
            r={10}
            circleStroke={circleStroke}
            circleStrokeWidth={1}
            circleFill={circleFill}
            text={text}
            textX={"50%"}
            textY={"50%"}
            textFontSize={16}
            textFontWeight={"bold"}
            textStrokeWidth={5}
            textFill={textFill}
        />
    )
}

function SVGStandardCircleButtonStyle() {
    return "SmallButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... "
}

function SVGButtonLight(text) {
    return StandardSVGCircleButton(text, dark, dark, light)
}

function SVGButtonLightDisabled(text) {
    return StandardSVGCircleButton(text, disabled, disabled, light)
}

function SVGButtonDark(text) {
    return StandardSVGCircleButton(text, light, dark, dark)
}

function SVGButtonDarkDisabled(text) {
    return StandardSVGCircleButton(text, light, disabled, disabled)
}

function SVGInfoButtonLight() {
    return SVGButtonLight("i")
}

function SVGInfoButtonLightDisabled() {
    return SVGButtonLightDisabled("i")
}

function SVGInfoButtonDark() {
    return SVGButtonDark("i")
}

function SVGInfoButtonDarkDisabled() {
    return SVGButtonDarkDisabled("i")
}

function SVGBuild1ButtonLight() {
    return SVGButtonLight("1")
}

function SVGBuild1ButtonLightDisabled() {
    return SVGButtonLightDisabled("1")
}

function SVGBuild1ButtonDark() {
    return SVGButtonDark("1")
}

function SVGBuild1ButtonDarkDisabled() {
    return SVGButtonDarkDisabled("1")
}

function SVGBuild2ButtonLight() {
    return SVGButtonLight("2")
}

function SVGBuild2ButtonLightDisabled() {
    return SVGButtonLightDisabled("2")
}

function SVGBuild2ButtonDark() {
    return SVGButtonDark("2")
}

function SVGBuild2ButtonDarkDisabled() {
    return SVGButtonDarkDisabled("2")
}

function SVGBuild3ButtonLight() {
    return SVGButtonLight("3")
}

function SVGBuild3ButtonLightDisabled() {
    return SVGButtonLightDisabled("3")
}

function SVGBuild3ButtonDark() {
    return SVGButtonDark("3")
}

function SVGBuild3ButtonDarkDisabled() {
    return SVGButtonDarkDisabled("3")
}

function SVGSlotButton(text, textFill, circleStroke, circleFill, circleStrokeDashArray) {
    return (
        <SVGCircleButton
            width={40}
            height={40}
            cx={20}
            cy={20}
            r={20}
            circleStroke={circleStroke}
            circleStrokeWidth={1}
            circleStrokeDashArray={circleStrokeDashArray}
            circleFill={circleFill}
            text={text}
            textX={"50%"}
            textY={"50%"}
            textFontSize={22}
            textFontWeight={"bold"}
            textStrokeWidth={5}
            textFill={textFill}
        />
    )
}

function SVGSlotButtonStyle() {
    return "SlotButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... "
}

function SVGSlotButtonUnslotted(text) {
    return SVGSlotButton(text, disabled, disabled, light, 8)
}

function SVGSlotButtonPrimary(text) {
    return SVGSlotButton(text, dark, dark, "#e0e0ff", 0)
}

function SVGSlotButtonSecondary(text) {
    return SVGSlotButton(text, dark, dark, "#ffe0e0", 0)
}

function SVGSlotButtonPool(text) {
    return SVGSlotButton(text, dark, dark, "#ffffe0", 0)
}

function SVGSlotButtonEpic(text) {
    return SVGSlotButton(text, dark, dark, "#ffd87f", 0)
}

function SVGSlotButtonInherent(text) {
    return SVGSlotButton(text, dark, dark, "#e0ffe0", 0)
}

export default SVGCircleButton;
export { StandardSVGCircleButton, SVGStandardCircleButtonStyle, SVGButtonLight, SVGButtonLightDisabled, SVGButtonDark, SVGButtonDarkDisabled };
export { SVGInfoButtonLight, SVGInfoButtonLightDisabled, SVGInfoButtonDark, SVGInfoButtonDarkDisabled };
export { SVGBuild1ButtonLight, SVGBuild1ButtonLightDisabled, SVGBuild1ButtonDark, SVGBuild1ButtonDarkDisabled };
export { SVGBuild2ButtonLight, SVGBuild2ButtonLightDisabled, SVGBuild2ButtonDark, SVGBuild2ButtonDarkDisabled };
export { SVGBuild3ButtonLight, SVGBuild3ButtonLightDisabled, SVGBuild3ButtonDark, SVGBuild3ButtonDarkDisabled };
export { SVGSlotButton, SVGSlotButtonStyle, SVGSlotButtonUnslotted, SVGSlotButtonPrimary};
export { SVGSlotButtonSecondary, SVGSlotButtonPool, SVGSlotButtonEpic, SVGSlotButtonInherent};
