import { InfoMessageStore } from '../store/InfoMessageStore';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../css/toast.scss';

toast.configure()

let initInfoMessageDisplay = () => {
    InfoMessageStore.subscribe( 
        s => s.infoMessage, 
        newInfoMessage => {
            toast.info(newInfoMessage, { position: toast.POSITION.BOTTOM_RIGHT })
        }
    )
}

export default initInfoMessageDisplay;