import React from 'react';
import { PowerSetType } from "../../enums/PowerSetType";
import { SVGInfoButtonLightDisabled, SVGInfoButtonLight, SVGStandardCircleButtonStyle } from '../SVG/SVGCircleButton'
import './PowerButtons.scss';

// Props
// - powerSetType: powerSetType enum
function PowerButtons(props){
    const clickHandlerInfo = (event) => {  
        alert('Info clicked')
    }

    const getInfo = (powerSetType) => {
        if (powerSetType === PowerSetType.NO_POWER) {
            return (
                <div className="SmallButton" onClick={clickHandlerInfo}><SVGInfoButtonLightDisabled/></div>
            )
        } else {
            return (
                <div className={SVGStandardCircleButtonStyle()} onClick={clickHandlerInfo}><SVGInfoButtonLight/></div>
            )
        }
    }

    return (
        <div className="PowerButtons">
            {getInfo(props.powerSetType)}
        </div>
    )
}

export default PowerButtons;