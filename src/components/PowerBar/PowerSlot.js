import React, { useState } from "react";
import { PowerSetType } from "../../enums/PowerSetType";
import { SlotState } from "../../enums/SlotState";
import { SVGSlotButtonUnslotted, SVGSlotButtonPrimary, SVGSlotButtonSecondary } from '../SVG/SVGCircleButton'
import { SVGSlotButtonPool, SVGSlotButtonEpic, SVGSlotButtonInherent, SVGSlotButtonStyle } from '../SVG/SVGCircleButton'
import './PowerSlot.scss';

function Slot(props) {
    if (props.slotState === SlotState.UNSLOTTED)
        return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonUnslotted()}</div>
    else if (props.slotState === SlotState.NO_POWER)
        return <div className='SlotButton'>{SVGSlotButtonUnslotted()}</div>
    else if (props.powerSetType === PowerSetType.NO_POWER)
        return <div className='SlotButton'>{SVGSlotButtonUnslotted()}</div>
    else if (props.powerSetType === PowerSetType.PRIMARY)
        return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonPrimary()}</div>
    else if (props.powerSetType === PowerSetType.SECONDARY)
        return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonSecondary()}</div>
    else if (props.powerSetType === PowerSetType.POOL)
        return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonPool()}</div>
    else if (props.powerSetType === PowerSetType.EPIC)
        return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonEpic()}</div>

    return <div className={SVGSlotButtonStyle()}>{SVGSlotButtonInherent()}</div>
}

// Props
//  - slotState: See SlotState enum for values.
//  - powerSetType: See PowerSetType enum for values.
function PowerSlot(props){
    const [slotState, setSlotState] = useState(props.slotState);
    const [powerSetType] = useState(props.powerSetType);
    const clickHandler = (event) => {  
        if (slotState === SlotState.NO_POWER) {
            event.preventDefault(); 
            props.onSlotChanged(this, slotState)
            return
        } else if (slotState === SlotState.UNSLOTTED) {
            setSlotState(SlotState.SLOTTED)
            props.onSlotChanged(this, slotState)
        }
    }
    const rightClickHandler = (event) => {  
        if (slotState === SlotState.SLOTTED) {
            setSlotState(SlotState.UNSLOTTED)
            props.onSlotChanged(this, slotState)
        }
        event.preventDefault(); 
    }
    
    return (
        <div onClick={clickHandler} onContextMenu={rightClickHandler} className="PowerSlot">
            <Slot slotState={slotState} powerSetType={powerSetType}/>
        </div>
    )
}

export default PowerSlot;