import React, { useState } from 'react';
import { fetchDataFromServer } from '../../fetch/fetch'
import './PowerText.scss';

// Props
//  - powerName: Power name
//  - powerLevel: Power level
//  - onPowerTextChanged: Called when power text changes
function PowerText(props){
    let getPowerName = () => {
        if ('None' === props.powerName || !props.powerName)
            return 'Tap To Set Power'
        else
            return props.powerName
    }

    let getTextColor = () => {
        if ('None' === props.powerName || !props.powerName)
            return 'TextColorDisabled'
        else
            return 'TextColor'
    }

    const clickHandler = (event) => {  
        props.onPowerTextChanged(getPowerName())
    }

    const [powerName, setPowerName] = useState(getPowerName());
    getPower(props.powerName, setPowerName)

    return (
        <div className="PowerText" onClick={clickHandler}>
            <div className={"PowerTextName " + getTextColor()}>{powerName}</div>
            <div className={"PowerTextLevel " + getTextColor()}>Level {props.powerLevel}</div>
        </div>
    )
}

let getPower = async (powerId, setPowerName) => {
    if ('None' === powerId || !powerId)
        return
    let json = await fetchDataFromServer('power/' + powerId)
    if (!json)
        return
    setPowerName(json.display_name)
}

export default PowerText;