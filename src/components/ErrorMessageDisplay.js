import { ErrorMessageStore } from '../store/ErrorMessageStore';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../css/toast.scss';

toast.configure()

let initErrorMessageDisplay = () => {
    ErrorMessageStore.subscribe( 
        s => s.errorMessage, 
        newErrorMessage => {
            toast.error(newErrorMessage, { position: toast.POSITION.BOTTOM_RIGHT })
        }
    )
}

export default initErrorMessageDisplay;