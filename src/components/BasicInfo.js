import React, { useState } from 'react';
import { fetchDataFromServer } from '../fetch/fetch'
import { ToonStore } from '../store/ToonStore';
import './BasicInfo.scss';

// Props
//  - onNameSetClick: Name click function
//  - onPowerSetClicked: Powerset click function
let Build = (props) => {
    const toon = ToonStore.useState(s => s.toon);
    const [archetypeName, setArchetypeName] = useState(toon.archetype_id);
    getArchetype(toon.archetype_id, setArchetypeName)
    const [primaryName, setPrimaryName] = useState(toon.builds[toon.current_build].power_sets["primary"]);
    getPowerSet(toon.builds[toon.current_build].power_sets["primary"], setPrimaryName)
    const [secondaryName, setSecondaryName] = useState(toon.builds[toon.current_build].power_sets["secondary"]);
    getPowerSet(toon.builds[toon.current_build].power_sets["secondary"], setSecondaryName)

    const nameClickHandler = () => {
        alert("Name")
    }
    const getName = () => {
        if (toon.name)
            return toon.name
        else 
            return "Name"
    }
    const getPowerSetNames = () => {
        if ('None' === primaryName || !primaryName)
            return 'Tap to set Primary, Secondary'
        return primaryName + '/' + secondaryName
    }

    // Power Set Store
    ToonStore.subscribe( 
        s => s.toon, 
        newToon => {
            let primary = newToon.builds[newToon.current_build].power_sets["primary"]
            let secondary = newToon.builds[newToon.current_build].power_sets["secondary"]
            getPowerSet(primary, setPrimaryName)
            getPowerSet(secondary, setSecondaryName)
        }
    )

    return (
        <div className="BasicInfo">
            <div className="BasicInfoName" onClick={nameClickHandler}>
                {getName()}
            </div>
            <div className="BasicInfoArchetypeAndLevel">
                <div className="BasicInfoArchetype">
                    {archetypeName}
                </div>
                <div className="BasicInfoLevel">
                    Level {toon.level}
                </div>
            </div>
            <div className="BasicInfoPrimaryAndSecondaryPowerSets transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... " onClick={props.onPowerSetClicked}>
                {getPowerSetNames()}
            </div>
        </div>
    )
}

let getArchetype = async (archetypeId, setArchetypeName) => {
    let json = await fetchDataFromServer('archetype/' + archetypeId)
    if (!json)
        return
    setArchetypeName(json.display_name)
}

let getPowerSet = async (powerSetId, setPowerSetName) => {
    if ('None' === powerSetId || !powerSetId)
        return
    let json = await fetchDataFromServer('power_set/' + powerSetId)
    if (!json)
        return
    setPowerSetName(json.display_name)
}

export default Build;