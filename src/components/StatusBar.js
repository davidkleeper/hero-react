import React, { useState } from 'react';
import { StatusMessageStore } from '../store/StatusMessageStore';
import './StatusBar.scss';

let intervalId = -1;

let StatusBar = (props) => {
    const [currentStatusMessage, setCurrentStatusMessage] = useState('');
    const [statusBarClass, setStatusBarClass] = useState('StatusBarVisible');
    const startStatusBarHideTimer = () => {
        if (-1 !== intervalId) clearInterval(intervalId)
        intervalId = setTimeout(()=>{ 
            setStatusBarClass('StatusBarHidden')
        }, 1000);
    }    
    StatusMessageStore.subscribe( 
        s => s.statusMessage, 
        newStatusMessage => {
            setCurrentStatusMessage(newStatusMessage) 
            setStatusBarClass('StatusBarVisible')
            startStatusBarHideTimer()
        }
    )
    return (
        <div className={"StatusBar " + statusBarClass}>
            {currentStatusMessage}
        </div>
    )
}

export default StatusBar;