import React, { useState }from 'react'
import CircleLoader from "react-spinners/CircleLoader";
import './TextPicker.scss';
import { css } from "@emotion/core";

// Props
// title - Title
// data - Array of data
// getText - Function that takes data and an index as params and returns the data to display.
// getId - Function that takes data and an index as params and returns a unique Id.
// itemClickCallback - Function that takes ID and index. Called on item click.
const TextPicker = (props) => {
  let [currentSelection, setCurrentSelection] = useState(-1)
  let buildList = (data, getTextFunc, getIdFunc, onClickFunc) => {
    if (!data || !getTextFunc)
      return
    let list = []
    if (0 < data.length) {
      for (let index = 0; index < data.length; index++) {
        let text = getTextFunc(data, index)
        let handleClick = () => {
          setCurrentSelection(index)
          onClickFunc(getIdFunc(data, index), index)
        }
        let className = (index === currentSelection)? 'TextPickerListItemSelected' : 'TextPickerListItem'
        list.push(<div className={className} onClick={handleClick} key={index}>{text}</div>)
      }
    } else {
      const override = css`display: block; margin: 0 auto;`;
      list.push(<CircleLoader css={override} size={150} color={"#555555"} loading={true} />)
    }
    return list
  }
  let list = buildList(props.data, props.getTextFunc, props.getIdFunc, props.itemClickCallback)

  return (
    <div className="TextPicker">
        <div className="TextPickerTitleBar">
          <div className="TextPickerPowerSetTitle">{props.title}</div>
        </div>
        <div className="TextPickerList">
          {list}
        </div>
    </div>
  )
}

export default TextPicker