import React from "react";
import { SVGBuild1ButtonDark, SVGBuild2ButtonLight, SVGBuild3ButtonLight, SVGStandardCircleButtonStyle } from './SVG/SVGCircleButton'
import './Build.scss';

// Props
//  - onClick: Click function
//  - imgSource: Image url
//  - label: Button text
let Build = (props) => {
    const clickHandler1 = () => {
        alert("1")
    }
    const clickHandler2 = () => {
        alert("2")
    }
    const clickHandler3 = () => {
        alert("3")
    }

    return (
        <div className="Build">
            <div className="BuildButtons">
                <div className={SVGStandardCircleButtonStyle()} onClick={clickHandler1}><SVGBuild1ButtonDark/></div>
                <div className={SVGStandardCircleButtonStyle()} onClick={clickHandler2}><SVGBuild2ButtonLight/></div>
                <div className={SVGStandardCircleButtonStyle()} onClick={clickHandler3}><SVGBuild3ButtonLight/></div>
            </div>
            <div className="TextColor BuildText">Build</div>
       </div>
    )
}

export default Build;