import React, { useRef, useState, useEffect } from 'react'
import {postDataToServerWithJSONResponse} from '../fetch/fetch'
import { PowerStore } from '../store/PowerStore';
import { SVGInfoButtonLight, SVGStandardCircleButtonStyle } from './SVG/SVGCircleButton'
import TextPicker from "./TextPicker";
import { ToonStore } from '../store/ToonStore';
import { useLocation } from 'react-awesome-router';
import './PowerPicker.scss';

// State
//    Power sets
//    Powers
//    Selected primary id and store index
//    Selected secondary id and store index
//
// Store
//    Toon Store: When Select clicked, Toon store updated with selected power. 
//    PowerStore: When Select or Cancel clicked, visible state set to false and Power store updated 
//      with pickerVisible set to false. 
//
// Use Effect
//    Load available power sets and powers. Store them in Power Store.
//    Drag support
//
// Props
//    powerSetPickerCallback - Called after the power sets are selected. Used to close the picker.
//    primary_set_id
//    secondary_set_id
const PowerPicker = (props) => {
  // WASM support
  let {context} = useLocation();
  let wasm = context.wasm

  // Drag support
  const [drag_support_pressed, set_drag_support_pressed] = useState(false)
  const [drag_support_position, set_drag_support_position] = useState({x: 0, y: 0})
  const onMouseMove = (event) => {
    if (drag_support_pressed) {
      set_drag_support_position({
        x: drag_support_position.x + event.movementX,
        y: drag_support_position.y + event.movementY
      })
    }
  }

  // State
  const [powerSets, setPowerSets] = useState(PowerStore.powerSets)
  const [powers, setPowers] = useState(PowerStore.powers)
  const [selectedPowerSetId, setSelectedPowerSetId] = useState('')
  const [selectedPowerSetIndex, setSelectedPowerSetIndex] = useState(-1)
  const [selectedPowerId, setSelectedPowerId] = useState('')
  const [selectedPowerIndex, setSelectedPowerIndex] = useState(-1)
  const ref = useRef()

  // Toon store
  const toon = ToonStore.useState(s => s.toon);
  const updateToon = (toonJSON) => {
    let toonObj = (Object.prototype.toString.call(toonJSON) === "[object String]")? JSON.parse(toonJSON) : toonJSON
    ToonStore.update(s => { s.toon = toonObj; })
  }

  // Power Store
  PowerStore.subscribe( 
    s => s.powerSets, 
    newPowerSets => {
      setPowerSets(newPowerSets)
    }
  )
  PowerStore.subscribe( 
    s => s.powers, 
    newPowers => {
      setPowers(newPowers)
    }
  )

  // Use Effect
  // Power Set Store: Fetch primary and secondary power sets.
  // Drag Support: Monitor changes to position state and update DOM.
  useEffect(() => {
    fetchPowerSets(toon, props.primary_set_id, props.secondary_set_id)
    fetchPowers(toon)
    if (ref.current) {
      ref.current.style.transform = `translate(${drag_support_position.x}px, ${drag_support_position.y}px)`
    }
  }, [drag_support_position])

  // Appearance
  let getSelectButtonClass = () => {
    if (-1 === selectedPowerSetIndex || -1 === selectedPowerIndex)
      return 'PowerPickerButtonDisabled'
    else
      return 'PowerPickerButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... '
  }

  // Click events
  let onPrimaryClicked = (id, index) => {
    setSelectedPowerSetId(id)
    setSelectedPowerSetIndex(index)
  }
  let onSecondaryClicked = (id, index) => {
    setSelectedPowerId(id)
    setSelectedPowerIndex(index)
  }
  let onCancelClicked = (id, index) => {
    PowerStore.update(s => { s.pickerVisible = false })
    props.powerSetPickerCallback()
  }
  let onSelectClicked = () => {
    if (-1 === selectedPowerSetIndex || -1 === selectedPowerIndex)
      return

      /*
    let dataArray = [];
    dataArray.push(JSON.stringify(toon))
    dataArray.push(selectedPrimaryPowerId)
    let updatedToon = wasm.core_command_web("toon_set_primary!@#" + JSON.stringify(dataArray))
    dataArray[0] = updatedToon
    dataArray[1] = selectedSecondaryPowerId
    updatedToon = wasm.core_command_web("toon_set_secondary!@#" + JSON.stringify(dataArray))
    updateToon(updatedToon)
    props.powerSetPickerCallback()
    */
  }
  let onInfoClicked = (id, index) => {
    alert('Info')
  }

  // Functions for TextPicker
  let getArchetypeId = (data, index) => {
    return data[index].id
  }
  let getArchetypeDisplayName = (data, index) => {
    return data[index].display_name
  }

  return (
    <div
        className="PowerPicker"
        ref={ ref }
        onMouseMove={ onMouseMove }
        onMouseDown={ () => set_drag_support_pressed(true) }
        onMouseUp={ () => set_drag_support_pressed(false) }
       >
        <div className="PowerPickerTitlebar">
            <div className="PowerPickerTitle">Select Power</div>
            <div className={SVGStandardCircleButtonStyle()} onClick={onInfoClicked}><SVGInfoButtonLight/></div> 
        </div>
        <div className="PowerPickerBody">
            <TextPicker 
              data={powerSets} 
              getTextFunc={getArchetypeDisplayName} 
              getIdFunc={getArchetypeId} 
              itemClickCallback={onPrimaryClicked}
              title="Power Set"
            />
            <TextPicker 
              data={powers} 
              getTextFunc={getArchetypeDisplayName} 
              getIdFunc={getArchetypeId} 
              itemClickCallback={onSecondaryClicked}
              title="Power"
            />
        </div>
        <div className="PowerPickerButtons">
          <div className="PowerPickerButton transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-103 ... " onClick={onCancelClicked}>Cancel</div>
          <div className={getSelectButtonClass()} onClick={onSelectClicked}>Select</div>
        </div>
    </div>
  )
}

let fetchPowerSets = async (toon, primary_set_id, secondary_set_id) => {
  if (!toon || "" === toon) return
  if (!primary_set_id || "" === primary_set_id) return
  if (!secondary_set_id || "" === secondary_set_id) return
  const rawResponse = await postDataToServerWithJSONResponse('get_power_entries/x', JSON.stringify(toon));
  const content = await rawResponse.json()
  console.log(content)

  // PowerStore.update(s => { s.primaryPowers = data })

  // Problem:
  // Heroku rejects long urls. Can't use Percent Encoding.
  // Options:
  // Post
  // Move everything to server. User logs on, track sessions.
  // Move everything to client. Toon logic duplicated in all clients.
}

let fetchPowers = async (toon) => {
  // let data = await fetchDataFromServer('secondary_power_sets_for_archetype/' + archetype_id)
  // PowerStore.update(s => { s.secondaryPowers = data })
}

export default PowerPicker