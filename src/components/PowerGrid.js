import React, { useState } from 'react'
import {fetchDataFromServer, postDataToServerWithJSONResponse} from '../fetch/fetch'
import PowerBar from "../components/PowerBar/PowerBar";
import { PowerSetStore } from '../store/PowerSetStore';
import { PowerSetType } from "../enums/PowerSetType";
import { SlotState } from "../enums/SlotState";
import { ToonStore } from '../store/ToonStore';
import { useMediaQuery } from 'react-responsive'
import { useLocation } from 'react-awesome-router';
import { useStoreState } from "pullstate";
import './PowerGrid.scss';

let PowerGrid = () => {
    // WASM support
    let {context, params, setContext, setLocation} = useLocation();
    let wasm = context.wasm
    if (!wasm){
        setLocation('/Splash'); 
        window.location.reload();
        return;
    }

    // Fetch
    const fetchPowerSetForPower = async (powerId) => {
        const storePowerSetInfoAll = useStoreState(PowerSetStore, s => s.powerSetInfo);
        const storePowerSetInfo = storePowerSetInfoAll[powerId];
        if (storePowerSetInfo)
            return storePowerSetInfo.set_type

        let data = await fetchDataFromServer('power_set_for_power/' + powerId)
        console.log(JSON.stringify(data))
        storePowerSetInfoAll[powerId] = data
        PowerSetStore.update(s => { s.powerSetInfo = storePowerSetInfoAll; })
        return data
    }

    // Toon store
    ToonStore.subscribe( 
        s => s.toon, 
        newToon => { setToon(newToon) }
    )

    // Power Set store
    PowerSetStore.subscribe( 
        s => s.powerSetInfo, 
        newPowerSetInfo => { setPowerSetInfo(newPowerSetInfo) }
    )

    // State
    const [toon, setToon] = useState(useStoreState(ToonStore, s => s.toon))
    const [powerSetInfo, setPowerSetInfo] = useState(useStoreState(PowerSetStore, s => s.powerSetInfo))
    const [powerEntries, setPowerEntries] = useState(null)

    const getColumnCount = (columnWidth) => {
        let count = 0;
        while (4 > count) {
            let canDisplayRow = useMediaQuery(
                { query: '(min-device-width: ' + (columnWidth * (count + 1)) + 'px)'}
            )
            if (!canDisplayRow)
                break;
            count++
        }
        return count;
    }
    
    const buildPowerBar = async (powerEntry, level, key) => {
        let powerSet = (powerEntry)? await fetchPowerSetForPower(powerEntry.power_id) : null;
        let powerLevel = (powerEntry)? powerEntry.level : level
        let powerId = (powerEntry)? powerEntry.power_id : "None"
        let powerSetType = (powerEntry)? powerSet.set_type : PowerSetType.NO_POWER
        let slotState1 = SlotState.NO_POWER
        let slotState2 = SlotState.NO_POWER
        let slotState3 = SlotState.NO_POWER
        let slotState4 = SlotState.NO_POWER
        let slotState5 = SlotState.NO_POWER
        let slotState6 = SlotState.NO_POWER
        let enhancement1 = "None"
        let enhancement2 = "None"
        let enhancement3 = "None"
        let enhancement4 = "None"
        let enhancement5 = "None"
        let enhancement6 = "None"
    
        return(
            <PowerBar 
                powerName={powerId}
                powerLevel={powerLevel}
                powerSetType={powerSetType}
                slotState1={slotState1} 
                slotState2={slotState2} 
                slotState3={slotState3} 
                slotState4={slotState4} 
                slotState5={slotState5} 
                slotState6={slotState6} 
                enhancement1={enhancement1}
                enhancement2={enhancement2}
                enhancement3={enhancement3}
                enhancement4={enhancement4}
                enhancement5={enhancement5}
                enhancement6={enhancement6}
                key={key} 
            />
        );
    }
    const buildPowerBars = async (power_entries) => {
        console.log('buildPowerBars')
        let key = 0
        powerBars.push(await buildPowerBar(power_entries.level_1_primary, 1, key++))
        powerBars.push(await buildPowerBar(power_entries.level_1_secondary, 1, key++))
        powerBars.push(await buildPowerBar(power_entries.level_2, 2, key++))
        powerBars.push(await buildPowerBar(power_entries.level_4, 4, key++))
        powerBars.push(await buildPowerBar(power_entries.level_6, 6, key++))
        powerBars.push(await buildPowerBar(power_entries.level_8, 8, key++))
        powerBars.push(await buildPowerBar(power_entries.level_10, 10, key++))
        powerBars.push(await buildPowerBar(power_entries.level_12, 12, key++))
        powerBars.push(await buildPowerBar(power_entries.level_14, 14, key++))
        powerBars.push(await buildPowerBar(power_entries.level_16, 16, key++))
        powerBars.push(await buildPowerBar(power_entries.level_18, 18, key++))
        powerBars.push(await buildPowerBar(power_entries.level_20, 20, key++))
        powerBars.push(await buildPowerBar(power_entries.level_22, 22, key++))
        powerBars.push(await buildPowerBar(power_entries.level_24, 24,key++))
        powerBars.push(await buildPowerBar(power_entries.level_26, 26, key++))
        powerBars.push(await buildPowerBar(power_entries.level_28, 28, key++))
        powerBars.push(await buildPowerBar(power_entries.level_30, 30, key++))
        powerBars.push(await buildPowerBar(power_entries.level_32, 32, key++))
        powerBars.push(await buildPowerBar(power_entries.level_35, 35, key++))
        powerBars.push(await buildPowerBar(power_entries.level_38, 38, key++))
        powerBars.push(await buildPowerBar(power_entries.level_41, 41, key++))
        powerBars.push(await buildPowerBar(power_entries.level_44, 44, key++))
        powerBars.push(await buildPowerBar(power_entries.level_47, 47, key++))
        powerBars.push(await buildPowerBar(power_entries.level_49, 49, key++))
        powerBars.push(await buildPowerBar(power_entries.health, 1, key++))
        powerBars.push(await buildPowerBar(power_entries.stamina, 1, key++))
        if (toon.level > 1) powerBars.push(await buildPowerBar(power_entries.swift, 2, key++))
        if (toon.level > 1)powerBars.push(await buildPowerBar(power_entries.hurdle, 2, key++))
        if (toon.level > 1)powerBars.push(await buildPowerBar(power_entries.sprint, 2, key++))
        powerBars.push(await buildPowerBar(power_entries.brawl, 1, key++))
        powerBars.push(await buildPowerBar(power_entries.prestige_power_slide, 1, key++))
        if (toon.archetype_id === "Class_Peacebringer" || toon.archetype_id === "Class_Warshade" ) {
            powerBars.push(await buildPowerBar(power_entries.level_1_kheldian, 1, key++))
            if (toon.level >= 10) 
                powerBars.push(await buildPowerBar(power_entries.level_10_kheldian, 10, key++))
        }
     }

    const getColumn = (columnIndex, columnCount, powerBars, powersPerColumn, powerBarWidth) => {
        let sortedPowerBars = []
        for (var rowIndex = 0; rowIndex < powersPerColumn; rowIndex++) {
            var index = rowIndex + (columnIndex * powersPerColumn)
            if (index >= powerBars.length)
                break;
            var powerBar = powerBars[index]
            sortedPowerBars.push(powerBar)
        }
        return (
            <div>
                {sortedPowerBars}
            </div>
        )
    }

    // Calc columns
    const POWER_BAR_WIDTH = 280
    const columnCount = getColumnCount(POWER_BAR_WIDTH);
    let powerBars = []
    const fetchPowerEntries = async () => {
        const content = await postDataToServerWithJSONResponse('get_power_entries/x', JSON.stringify(toon))
        console.log(content);
        return content
    }
    const fetchAndBuildPowers = async (powerEntries, setPowerEntries) => {
        if (null == powerEntries)
            setPowerEntries(await fetchPowerEntries())
        else {
            buildPowerBars(await powerEntries)
        }
    }
    const powersPerColumn = Math.ceil(powerBars.length / columnCount)
    fetchAndBuildPowers(powerEntries, setPowerEntries);
    
    return (
        <div className="PowerGrid">
            <div className="PowerGridColumn">
                {getColumn(0, columnCount, powerBars, powersPerColumn, POWER_BAR_WIDTH)}
            </div>
            <div className="PowerGridColumn">
                {getColumn(1, columnCount, powerBars, powersPerColumn, POWER_BAR_WIDTH)}
            </div>
            <div className="PowerGridColumn">
                {getColumn(2, columnCount, powerBars, powersPerColumn, POWER_BAR_WIDTH)}
            </div>
            <div className="PowerGridColumn">
                {getColumn(3, columnCount, powerBars, powersPerColumn, POWER_BAR_WIDTH)}
            </div>
        </div>
    )
}

export default PowerGrid;